<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request; 
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class EmailController extends Controller
{
    public function store(Request $request)
    {

        Mail::send('contatoEmail', $request->all(), function ($msg) {
            $msg->subject('Vegefoods Pedido');
            $msg->to('camuelsantos@gmail.com');
        });
       return Redirect::to('/');

    }
}
