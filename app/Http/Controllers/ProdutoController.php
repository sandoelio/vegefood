<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $dados)
    {
        $produto1=$dados->blankCheckbox1;
        $quantidade1=$dados->campoUm;
        $valor1=$dados->totalImput1;

        $produto2=$dados->blankCheckbox2;
        $quantidade2=$dados->campoDois;
        $valor2=$dados->totalImput2;

        $produto3=$dados->blankCheckbox3;
        $quantidade3=$dados->campoTres;
        $valor3=$dados->totalImput3;

        $produto4=$dados->blankCheckbox4;
        $quantidade4=$dados->campoQuatro;
        $valor4=$dados->totalImput4;

        $produto5=$dados->blankCheckbox5;
        $quantidade5=$dados->campoCinco;
        $valor5=$dados->totalImput5;

        $produto6=$dados->blankCheckbox6;
        $quantidade6=$dados->campoSeis;
        $valor6=$dados->totalImput6;

        $subtotal=$dados->subtotalImput;

        return view('verificacao', compact('produto1','quantidade1',
        'produto2','quantidade2','produto3','quantidade3','produto4',
        'quantidade4','produto5','quantidade5','produto6','quantidade6',
        'subtotal','valor1','valor2','valor3','valor4','valor5','valor6'));
    }

    public function storeVerificacao(Request $dados)
    {
        $nome=$dados->nome;
        $sobrenome=$dados->sobrenome;
        $estado=$dados->estado;
        $cidade=$dados->cidade;
        $cep=$dados->cep;
        $tel=$dados->tel;
        $email=$dados->email;

        $produto1=$dados->produto1;
        $quantidade1=$dados->quantidade1;
        $produto2=$dados->produto2;
        $quantidade2=$dados->quantidade2;
        $produto3=$dados->produto3;
        $quantidade3=$dados->quantidade3;
        $produto4=$dados->produto4;
        $quantidade4=$dados->quantidade4;
        $produto5=$dados->produto5;
        $quantidade5=$dados->quantidade5;
        $produto6=$dados->produto6;
        $quantidade6=$dados->quantidade6;

        $optradio1=$dados->optradio1;
        $subtotaldados=$dados->subinput;
        return view('dados', compact('nome','sobrenome','estado','cidade','cep','tel',
        'email','produto1','quantidade1','produto2','quantidade2','produto3','quantidade3',
        'produto4','quantidade4','produto5','quantidade5','produto6','quantidade6',
        'optradio1','subtotaldados'));

    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
