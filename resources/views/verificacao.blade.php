@include("cabecalho")

    <!----------------Imagem------------------------->
    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Página</a></span></p>
            <h1 class="mb-0 bread">Verificação</h1>
          </div>
        </div>
      </div>
    </div>
    <!----------------formulario------------------------->
    <form action="/dados" class="billing-form" method="post" >
    {!! csrf_field() !!}
    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
        <div class="col-xl-7 ftco-animate">
				<h3 class="mb-4 billing-heading">Preencha os campos</h3>
	          	<div class="row align-items-end">
	          		<div class="col-md-6">
	                <div class="form-group">
	                	<label for="firstname">Nome</label>
	                  <input type="text" name="nome" id="nome" class="form-control">
	                </div>
	                </div>
	                <div class="col-md-6">
	                <div class="form-group">
	                    <label for="lastname">Sobrenome</label>
	                    <input type="text" name="sobrenome" id="sobrenome" class="form-control">
	                </div>
                </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
                        <label for="country">Cidade</label>
                        <div class="select-wrap">
                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                        <select name="cidade" id="cidade" class="form-control">
                            <option disabled selected>- Selecione -</option>
		                  	<option value="Salvador">Salvador</option>
		                    <option value="Lauro de Freitas">Lauro de Freitas</option>
		                    <option value="Camaçari">Camaçari</option>
		                  </select>
		                </div>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="towncity">Bairro</label>
	                  <input type="text" class="form-control" name="bairro" id="bairro">
	                </div>
		            </div>
		            <div class="col-md-6">
		            	<div class="form-group">
		            		<label for="postcodezip">CEP</label>
	                  <input type="text" class="form-control" name="cep" id="cep">
	                </div>
		            </div>
                    <div class="w-100">
		            	<div class="form-group">
		            		<label for="postcodezip">Logradouro</label>
	                  <input type="text" class="form-control" name="logradouro" id="logradouro">
	                </div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Número de contato</label>
	                  <input type="text" class="form-control" name="tel" id="tel" placeholder="">
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="emailaddress">Email</label>
	                  <input type="email" class="form-control" name="email" id="email"  placeholder="email@email.com">
	                </div>
                </div>
	            </div>
            </div>

            <!----------------Produtos------------------------->
                <div class="col-xl-5">
	            <div class="row mt-5 pt-3">
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Produto / Quantidade</h3>
	          			<p class="d-flex">
                          <input type="hidden" name="produto1" id="produto1" value="{{$produto1}}"></input>
                          <span>{{$produto1}}</span>
                          <input type="hidden" name="quantidade1" id="quantidade1" value="{{$quantidade1}}"></input>
                          <span>{{$quantidade1}}</span>
                          <input type="hidden" name="valor1" id="valor1" value="{{$valor1}}"></input>
                          <!--<span>R$ {{$valor1}}</span>-->
                        </p>
                        <!----------------------------------------->
                        <p class="d-flex">
                          <input type="hidden" name="produto2" id="produto2" value="{{$produto2}}"></input>
                          <span>{{$produto2}}</span>
                          <input type="hidden" name="quantidade2" id="quantidade2" value="{{$quantidade2}}"></input>
                          <span>{{$quantidade2}}</span>
                          <input type="hidden" name="valor2" id="valor2" value="{{$valor2}}"></input>
                          <!--<span>R$ {{$valor2}}</span>-->
                        </p>
                        <!----------------------------------------->
                        <p class="d-flex">
                          <input type="hidden" name="produto3" id="produto3" value="{{$produto3}}"></input>
                          <span>{{$produto3}}</span>
                          <input type="hidden" name="quantidade3" id="quantidade3" value="{{$quantidade3}}"></input>
                          <span>{{$quantidade3}}</span>
                          <input type="hidden" name="valor3" id="valor3" value="{{$valor3}}"></input>
                          <!--<span>R$ {{$valor3}}</span>-->
                        </p>
                        <!----------------------------------------->
                        <p class="d-flex">
                          <input type="hidden" name="produto4" id="produto4" value="{{$produto4}}"></input>
                          <span>{{$produto4}}</span>
                          <input type="hidden" name="quantidade4" id="quantidade4" value="{{$quantidade4}}"></input>
                          <span>{{$quantidade4}}</span>
                          <input type="hidden" name="valor4" id="valor4" value="{{$valor4}}"></input>
                          <!--<span>R$ {{$valor4}}</span>-->
                        </p>
                        <!----------------------------------------->
                        <p class="d-flex">
                          <input type="hidden" name="produto5" id="produto5" value="{{$produto5}}"></input>
                          <span>{{$produto5}}</span>
                          <input type="hidden" name="quantidade5" id="quantidade5" value="{{$quantidade5}}"></input>
                          <span>{{$quantidade5}}</span>
                          <input type="hidden" name="valor5" id="valor5" value="{{$valor5}}"></input>
                          <!--<span>R$ {{$valor5}}</span>-->
                        </p>
                        <!----------------------------------------->
                        <p class="d-flex">
                          <input type="hidden" name="produto6" id="produto6" value="{{$produto6}}"></input>
                          <span>{{$produto6}}</span>
                          <input type="hidden" name="quantidade6" id="quantidade6" value="{{$quantidade6}}"></input>
                          <span>{{$quantidade6}}</span>
                          <input type="hidden" name="valor6" id="valor6" value="{{$valor6}}"></input>
                          <!--<span>R$ {{$valor6}}</span>-->
                        </p>
                        <hr>
                    <!----------------subtotal------------------------->
                    <input type="hidden" name="subinput" id="subinput" value="{{$subtotal}}"></input>
                        <p class="d-flex total-price">
                            <span>Subtotal</span>
                            <td> R$ <span>{{$subtotal}}</span></td>
                        </p>
                    </div>
	          	</div>
                <!----------------Fim Produtos------------------------->
	            <!----------------pagamento------------------------->

	          	<div class="cart-detail p-4 p-md-2">
                    <h3 class="billing-heading mb-4">Pagamento</h3>
                <div class="form-group">
                    <select class="form-control" id="optradio1" name="optradio1">
                        <option disabled selected>- Selecione -</option>
                        <option value="Cartão de credito">Cartão de credito</option>
                        <option value="Á vista dinheiro">Á vista dinheiro</option>
                    </select>
                </div>
                </div>
	          	</div>
	          </div>
          </div>
        </div>

        <center>
        <button type="submit" id="modalVerificacao" class="btn btn-primary col-5 text-center ">Enviar seu Pedido</button>
        </center>
    </form>
    </section>

        <!----------------Fim Formulario------------------->
        <!----------------Fim pagamento------------------------->
    @include('rodape')
  </body>
</html>
