@include('cabecalho')

<!-- Banner -->
<section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
        <div class="slider-item" style="background-image: url(images/bg_1.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
            <div class="col-md-12 ftco-animate text-center">
                <h1 class="mb-2">Servimos vegetais frescos &amp; Frutas</h1>
                <h2 class="subheading mb-4">Entregamos vegetais orgânicos &amp; Frutas</h2>
                <!--<p><a href="#" class="btn btn-primary">Ver detalhes</a></p>-->
            </div>
            </div>
        </div>
        </div>

        <div class="slider-item" style="background-image: url(images/bg_2.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
            <div class="col-sm-12 ftco-animate text-center">
                <h1 class="mb-2">100% Fresco &amp; Alimentos orgânicos</h1>
                <h2 class="subheading mb-4">Entregamos vegetais orgânicos &amp; Frutas</h2>
                <!--<p><a href="#" class="btn btn-primary">Ver detalhes</a></p>-->
            </div>
            </div>
        </div>
        </div>
    </div>
</section>
<!-- Fim Banner -->
<!-- Serviços -->
<section class="ftco-section">
<div class="container">
<div class="row no-gutters ftco-services">
    <!--bloco 1-->
    <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services mb-md-0 mb-4">
            <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
                <span class="flaticon-shipped"></span>
            </div>
            <div>
                <h3 class="heading">Envio Grátis</h3>
                <span>No pedido acima R$ 100</span>
            </div>
        </div>
    </div>
    <!--bloco 2-->
    <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services mb-md-0 mb-4">
            <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
                <span class="flaticon-diet"></span>
            </div>
            <div>
                <h3 class="heading">Sempre fresco</h3>
                <span>Produto empacotado</span>
            </div>
        </div>
    </div>
    <!--bloco 3-->
    <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services mb-md-0 mb-4">
            <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
                    <span class="flaticon-award"></span>
            </div>
            <div>
                <h3 class="heading">Qualidade superior</h3>
                <span>Produtos de qualidade</span>
            </div>
        </div>
    </div>
    <!--bloco 4-->
    <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
        <div class="media block-6 services mb-md-0 mb-4">
            <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
                <span class="flaticon-customer-service"></span>
            </div>
            <div>
                <h3 class="heading">Suporte</h3>
                <span>24/7 Suporte</span>
            </div>
        </div>
    </div>
</div>
</div>
</section>
<!-- Fim Serviços -->
<!-- Menu card -->
<section class="ftco-section ftco-category ftco-no-pt">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6 order-md-last align-items-stretch d-flex">
                        <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex" style="background-image: url(images/category.jpg);">
                            <div class="text text-center">
                                <h2>Protege a saúde</h2>
                                <p><a href="/lista" class="btn btn-primary">Compre Agora</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                    <a href="/lista/legume">
                        <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image: url(images/category-1.jpg);">
                            <div class="text px-3 py-1">
                                <h2 class="mb-2" style="color:#ffff">Legumes</h2>
                            </div>
                        </div>
                    </a>
                    <a href="/lista/fruta">
                        <div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image: url(images/category-2.jpg);">
                            <div class="text px-3 py-1">
                                <h2 class="mb-0" style="color:#ffff">Frutas</h2>
                            </div>
                        </div>
                    </a>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
            <a href="/lista/suco">
                <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end" style="background-image: url(images/category-3.jpg);">
                    <div class="text px-3 py-1">
                        <h2 class="mb-0" style="color:#ffff">Sucos</h2>
                    </div>
                </div>
            </a>
            <a href="/lista/seca">
                <div class="category-wrap ftco-animate img d-flex align-items-end" style="background-image: url(images/category-4.jpg);">
                    <div class="text px-3 py-1">
                        <h2 class="mb-0" style="color:#ffff">Secas</h2>
                    </div>
                </div>
            </a>
            </div>
        </div>
    </div>
</section>
<!-- Fim Menu card -->
<!-- Menu produtos -->
<section class="ftco-section">
    <div class="container col-md-12 heading-section text-center ftco-animate">
            <h2>Produtos em destaques</h2>
    </div>
    <div class="container">
        <div class="row">
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/legume" class="img-prod"><img class="img-fluid" src="images/product-1.jpg" >
                        <span class="status">30%</span>
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Pimentão - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span class="mr-2 price-dc">R$120.00</span><span class="price-sale">R$80.00</span></p>
                            </div>
                        </div>
                           <!-- <p><a href="/loja" class="btn btn-primary">Faça seu Pedido</a></p>
                            <div class="bottom-area d-flex px-3">
                            <div class="m-auto d-flex">
                                <a href="/produto" class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                    <img src="https://img.icons8.com/color/20/000000/visible.png">
                                </a>
                                <a href="/carrinho" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                    <span><i class="ion-ios-cart"></i></span>
                                </a>
                                <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                    <span><i class="ion-ios-heart"></i></span>
                                </a>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/fruta" class="img-prod"><img class="img-fluid" src="images/product-2.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Morango - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/seca" class="img-prod"><img class="img-fluid" src="images/product-3.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Feijão verde - 2 centro</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/legume" class="img-prod"><img class="img-fluid" src="images/product-4.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Repolho roxo - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/fruta" class="img-prod"><img class="img-fluid" src="images/product-5.jpg" >
                        <span class="status">30%</span>
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Tomate - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span class="mr-2 price-dc">R$120.00</span><span class="price-sale">R$80.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/legume" class="img-prod"><img class="img-fluid" src="images/product-6.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Brocolis - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/legume" class="img-prod"><img class="img-fluid" src="images/product-7.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Cenoura - 100UN</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <!--------------------------------------------------------------------->
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/lista/suco" class="img-prod"><img class="img-fluid" src="images/product-8.jpg" >
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3>Suco da Fruta - 100UN com 500ml cada</h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span>R$120.00</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Fim Menu produtos -->
<!--Banner do cronometro-->
    <section class="ftco-section img" style="background-image: url(images/bg_3.jpg);">
    <div class="container">
            <div class="row justify-content-end">
        <div class="col-md-6 heading-section ftco-animate deal-of-the-day ftco-animate">
        <span class="subheading">Melhor Preço Para Você</span>
        <h2 class="mb-4">Acordo do dia</h2>
        <p>Olhe para o cronômetro e nao perca a oportunidade</p>
        <h3><a href="#">Espinafre</a></h3>
        <span class="price">R$10,00 <a href="#">agora R$5,00</a></span>
        <div id="timer" class="d-flex mt-5">
            <div class="time" id="days"></div>
            <div class="time pl-3" id="hours"></div>
            <div class="time pl-3" id="minutes"></div>
            <div class="time pl-3" id="seconds"></div>
        </div>
        </div>
    </div>
    </div>
</section>
<!-- Fim Banner do cronometro-->
<!-- Depoimentos-->
<section class="ftco-section testimony-section">
    <div class="container">
    <div class="row justify-content-center mb-5 pb-3">
        <div class="col-md-7 heading-section ftco-animate text-center">
        <span class="subheading">Depoimentos</span>
        <h2 class="mb-4">Nosso cliente satisfeito diz:</h2>
        <p>Aproveitem as compras os produtos são de ótimas qualidades</p>
        </div>
    </div>
    <!------------------------------------------------------------------------->
    <div class="row ftco-animate">
        <div class="col-md-12">
        <div class="carousel-testimony owl-carousel">
            <div class="item">
            <div class="testimony-wrap p-4 pb-5">
                <div class="user-img mb-5" style="background-image: url(images/person_1.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                </span>
                </div>
                <div class="text text-center">
                <p class="mb-1 pl-4 line">Depoimentos</p>
                <p class="name">Cliente</p>
                <span class="position">Profissão</span>
                </div>
            </div>
            </div>
    <!------------------------------------------------------------------------->
            <div class="item">
            <div class="testimony-wrap p-4 pb-5">
                <div class="user-img mb-5" style="background-image: url(images/person_2.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                </span>
                </div>
                <div class="text text-center">
                <p class="mb-1 pl-4 line">Depoimentos</p>
                <p class="name">Cliente</p>
                <span class="position">Profissão</span>
                </div>
            </div>
            </div>
    <!------------------------------------------------------------------------->
            <div class="item">
            <div class="testimony-wrap p-4 pb-5">
                <div class="user-img mb-5" style="background-image: url(images/person_3.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                </span>
                </div>
                <div class="text text-center">
                <p class="mb-1 pl-4 line">Depoimentos</p>
                <p class="name">Cliente</p>
                <span class="position">Profissão</span>
                </div>
            </div>
            </div>
    <!------------------------------------------------------------------------->
            <div class="item">
            <div class="testimony-wrap p-4 pb-5">
                <div class="user-img mb-5" style="background-image: url(images/person_1.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                    <i class="icon-quote-left"></i>
                </span>
                </div>
                <div class="text text-center">
                <p class="mb-1 pl-4 line">Depoimentos</p>
                <p class="name">Cliente</p>
                <span class="position">Profissão</span>
                </div>
            </div>
            </div>
    <!------------------------------------------------------------------------->
            <div class="item">
                <div class="testimony-wrap p-4 pb-5">
                    <div class="user-img mb-5" style="background-image: url(images/person_1.jpg)">
                    <span class="quote d-flex align-items-center justify-content-center">
                        <i class="icon-quote-left"></i>
                    </span>
                    </div>
                    <div class="text text-center">
                    <p class="mb-1 pl-4 line">Depoimentos</p>
                    <p class="name">Cliente</p>
                    <span class="position">Profissão</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</section>
<!-- Fim depoimentos-->
<hr>
<!-- Logos clientes-->
<section class="ftco-section ftco-partner">
    <div class="container">
        <div class="col-sm ftco-animate">
            <span style="color: green"> Nossos clientes:</span>
        </div>
        <div class="row">
            <div class="col-sm ftco-animate">
                <a href="https://www.saladaprontaentrega.com.br/" class="partner"><img src="images/salada.png" class="img-fluid" ></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="http://redeforte.com/site/" class="partner"><img src="images/forte.png" class="img-fluid" ></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="https://www.spassosabores.com.br/" class="partner"><img src="images/sabores.png" class="img-fluid" ></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/marmita.png" class="img-fluid" ></a>
            </div>
            <div class="col-sm ftco-animate">
                <a href="#" class="partner"><img src="images/cereja.png" class="img-fluid" ></a>
            </div>
        </div>
    </div>
</section>
<!-- Fim Logos clientes-->
<!-- Menu email inscrição-->
<section class="ftco-section ftco-no-pt ftco-no-pb py-5 bg-light">
    @include('email')
</section>
<!-- Fim Menu email inscrição-->
    @include('rodape')
