<!DOCTYPE html>
<html lang="pt">
<head>
<title>Vegefoods</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">

<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="css/jquery.timepicker.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>
<link rel="stylesheet" href="css/flaticon.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/style.css">

</head>
<body class="goto-here">
<!--Menu de informações-->
<div class="py-1 bg-primary">
    <div class="container">
        <div class="row no-gutters d-flex align-items-start align-items-center px-md-0">
            <div class="col-lg-12 d-block">
                <div class="row d-flex">
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><img src="https://img.icons8.com/offices/20/000000/whatsapp.png"></div>
                        <!--<div class="icon mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>-->
                        <span class="text">(71)993775830</span>
                    </div>
                    <div class="col-md pr-4 d-flex topper align-items-center">
                        <div class="icon mr-2 d-flex justify-content-center align-items-center"><img src="https://img.icons8.com/color/20/000000/filled-message.png"></div>
                        <span class="text">vegefoods@email.com</span>
                    </div>
                    <div class="col-md-5 pr-4 d-flex topper align-items-center ">
                    <div class="icon mr-2 d-flex justify-content-center align-items-center"><img src="https://img.icons8.com/color/20/000000/alarm.png"></div>
                        <span class="text">3-5 DIAS ÚTEIS ENTREGA &amp; DEVOLUÇÕES GRATUITAS</span>
                    </div>
                </div>
            </div>
        </div>
        </div>
</div>
<!--Fim do menu informações-->
<!--menu principal-->
<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="/index">Vegefoods</a>
        <!-- <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
        </button>-->

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active"><a href="/index" class="nav-link">Principal</a></li>
               <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown04" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Vendas</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown04">
                        <a class="dropdown-item" href="loja">Loja</a>
                        <a class="dropdown-item" href="lista">Lista de Desejos</a>
                        <a class="dropdown-item" href="produto">Produto único</a>
                        <a class="dropdown-item" href="carrinho">Carrinho</a>
                        <a class="dropdown-item" href="verificacao">Verificação de saída</a>
                    </div>
                </li>-->
               <!-- <li class="nav-item"><a href="sobre" class="nav-link">Sobre</a></li>
                <li class="nav-item"><a href="blog" class="nav-link">Blog</a></li>-->
                <li class="nav-item"><a href="contato" class="nav-link">Contato</a></li>
                <!--<li class="nav-item cta cta-colored"><a href="cart" class="nav-link"><span class="icon-shopping_cart"></span>[0]</a></li>-->
            </ul>
        </div>
    </div>
    </nav>
