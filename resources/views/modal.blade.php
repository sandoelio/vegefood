<!-- Modal grande -->
<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">


    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index.html">Página</a></span></p>
            <h1 class="mb-0 bread">Verificação</h1>
          </div>
        </div>
      </div>
    </div>
<!----------------formulario------------------------->
<form action="" class="billing-form" method="post" >
    {!! csrf_field() !!}
    <section class="ftco-section">
      <div class="container">
        <div class="row justify-content-center">
        <div class="col-xl-7 ftco-animate">
				<h3 class="mb-4 billing-heading">Preencha os campos</h3>
	          	<div class="row align-items-end">
	          		<div class="col-md-6">
	                <div class="form-group">
	                	<label for="firstname">Nome</label>
	                  <input type="text" name="nome" id="nome" class="form-control">
	                </div>
	                </div>
	                <div class="col-md-6">
	                <div class="form-group">
	                    <label for="lastname">Sobrenome</label>
	                    <input type="text" name="sobrenome" id="sobrenome" class="form-control">
	                </div>
                </div>
                <div class="w-100"></div>
		            <div class="col-md-12">
		            	<div class="form-group">
                        <label for="country">Estado</label>
                        <div class="select-wrap">
                        <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                        <select name="estado" id="estado" class="form-control">
                            <option disabled selected>- Selecione -</option>
		                  	<option value="Bahia">Bahia</option>
		                    <option value="Maranhão">Maranhão</option>
		                    <option value="Pernambuco">Pernambuco</option>
		                    <option value="Ceara">Ceara</option>
		                  </select>
		                </div>
		            	</div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
		            	<div class="form-group">
	                	<label for="towncity">Cidade</label>
	                  <input type="text" class="form-control" name="cidade" id="cidade">
	                </div>
		            </div>
		            <div class="col-md-6">
		            	<div class="form-group">
		            		<label for="postcodezip">CEP</label>
	                  <input type="text" class="form-control" name="cep" id="cep">
	                </div>
		            </div>
		            <div class="w-100"></div>
		            <div class="col-md-6">
	                <div class="form-group">
	                	<label for="phone">Número de contato</label>
	                  <input type="text" class="form-control" name="tel" id="tel" placeholder="">
	                </div>
	              </div>
	              <div class="col-md-6">
	                <div class="form-group">
	                	<label for="emailaddress">Email</label>
	                  <input type="text" class="form-control" name="email" id="email"  placeholder="">
	                </div>
                </div>
	            </div>
            </div>

            <!----------------Produtos------------------------->
                <div class="col-xl-5">
	            <div class="row mt-5 pt-3">
	          	<div class="col-md-12 d-flex mb-5">
	          		<div class="cart-detail cart-total p-3 p-md-4">
	          			<h3 class="billing-heading mb-4">Produto / Quantidade</h3>
	          			<p class="d-flex">
                          <input type="hidden" name="produto1" id="produto1" value="{{$produto1}}"></input>
                          <span>{{$produto1}}</span>
                          <input type="hidden" name="quantidade1" id="quantidade1" value="{{$quantidade1}}"></input>
                          <span>{{$quantidade1}}</span>
                        </p>
                        <hr>
                        <p class="d-flex total-price">
                            <span>Total</span>
                            <span name="valor" id="valor">$17.60</span>
                        </p>
                    </div>
	          	</div>
            <!----------------Fim Produtos------------------------->
	        <!----------------pagamento------------------------->

	          	<div class="cart-detail p-3 p-md-4">
                    <h3 class="billing-heading mb-4">Meio de pagamento</h3>
                <div class="form-group">
                    <select class="form-control" id="optradio1" name="optradio1">
                        <option disabled selected>- Selecione -</option>
                        <option value="Boleto">Boleto</option>
                        <option value="Paypal">Paypal</option>
                        <option value="Deposito Bancário">Deposito Bancário</option>
                    </select>
                </div>
                </div>
	          	</div>
	          </div>
          </div>
        </div>

        <center>
        <button type="button" id="modalVerificacao" class="btn btn-primary col-5 text-center" data-toggle="modal" data-target=".bd-example-modal-lg">Enviar seu Pedido</button>
        </center>
    </form>
    </section>

    <!----------------Fim Formulario------------------->
    <!----------------Fim pagamento------------------------->

    </div>
  </div>
</div>
