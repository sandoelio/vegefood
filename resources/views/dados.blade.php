<!DOCTYPE html>
<html lang="pt">
<head>
<title>Vegefoods</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

<link rel="stylesheet" href="css/open-iconic-bootstrap.min.css">
<link rel="stylesheet" href="css/animate.css">

<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/owl.theme.default.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">

<link rel="stylesheet" href="css/aos.css">

<link rel="stylesheet" href="css/ionicons.min.css">

<link rel="stylesheet" href="css/bootstrap-datepicker.css">
<link rel="stylesheet" href="css/jquery.timepicker.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="css/flaticon.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/style.css">
</head>


<body class="goto-here">
<?php
    $nome = $_POST["nome"];
    $sobrenome = $_POST["sobrenome"];
    $cidade = $_POST["cidade"];
    $bairro = $_POST["bairro"];
    $logradouro = $_POST["logradouro"];
    $cep = $_POST["cep"];
    $tel = $_POST["tel"];
    $email = $_POST["email"];

    $produto1 = $_POST["produto1"];
    $quantidade1= $_POST["quantidade1"];
    $produto2 = $_POST["produto2"];
    $quantidade2= $_POST["quantidade2"];
    $produto3 = $_POST["produto3"];
    $quantidade3= $_POST["quantidade3"];
    $produto4 = $_POST["produto4"];
    $quantidade4= $_POST["quantidade4"];
    $produto5 = $_POST["produto5"];
    $quantidade5= $_POST["quantidade5"];
    $produto6 = $_POST["produto6"];
    $quantidade6= $_POST["quantidade6"];

    $optradio1= $_POST["optradio1"];
    //$subtotaldados = $_POST["subtotaldados"];


?>
<div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
          	<p class="breadcrumbs"><span class="mr-2"><a href="index">Página</a></span></p>
            <h1 class="mb-0 bread">Finalização</h1>
          </div>
        </div>
      </div>
</div>
<br>
<br>
<hr>
{!! Form:: open(['route'=>'mail.store', 'method' => 'POST']) !!}
{!! csrf_field() !!}
<center><p style="font-size:20px"><b>Dados do envio.</b></p>
        <strong>NOME/SOBRENOME: </strong><input type="hidden" value="{!! $nome !!} {!! $sobrenome !!}"name="nome"><span>{!! $nome !!} {!! $sobrenome !!}</span></input><br>
        <strong>CIDADE/BAIRRO:  </strong><input type="hidden" value=" {!! $cidade !!} / {!!$bairro!!}" name="cidade"><span>{!! $cidade !!} / {!!$bairro!!}</span></input><br>
        <strong>LOGRADOURO:  </strong><input  type="hidden"value=" {!! $logradouro !!}" name="logradouro"><span>{!! $logradouro !!}</span></input><br>
        <strong>CEP: </strong><input type="hidden" value=" {!! $cep !!}" name="cep"><span>{!! $cep !!}</span></input><br>
        <strong>CONTATO: </strong><input type="hidden" value=" {!! $tel !!}" name="tel" readonly><span>{!! $tel !!}</span></input><br>
        <strong>EMAIL: </strong><input type="hidden" value=" {!! $email !!}" name="email" readonly><span>{!! $email !!}</span></input><br>

        <strong>PRODUTO/QUANTIDADE:</strong><br>
        <input type="hidden" value=" {!! $produto1 !!}  {!! $quantidade1 !!}" name="produto1"><span>{!! $produto1 !!}  {!! $quantidade1 !!}</span></input><br>
        <input type="hidden" value=" {!! $produto2 !!}  {!! $quantidade2 !!}" name="produto2"><span>{!! $produto2 !!}  {!! $quantidade2 !!}</span></input><br>
        <input type="hidden" value=" {!! $produto3 !!}  {!! $quantidade3 !!}" name="produto3"><span>{!! $produto3 !!}  {!! $quantidade3 !!}</span></input><br>
        <input type="hidden" value=" {!! $produto4 !!}  {!! $quantidade4 !!}" name="produto4"><span>{!! $produto4 !!}  {!! $quantidade4 !!}</span></input><br>
        <input type="hidden" value=" {!! $produto5 !!}  {!! $quantidade5 !!}" name="produto5"><span>{!! $produto5 !!}  {!! $quantidade5 !!}</span></input><br>
        <input type="hidden" value=" {!! $produto6 !!}  {!! $quantidade6 !!}" name="produto6"><span>{!! $produto6 !!}  {!! $quantidade6 !!}</span></input><br>
        <strong>TOTAL: R$ </strong><input type="hidden" value=" {!! $subtotaldados !!}" name="subtotaldados"><span>{!! $subtotaldados !!}</span></input><br>
        <strong>FORMA DE PAGAMENTO: </strong><input type="hidden" value=" {!! $optradio1 !!}" name="optradio1"><span>{!! $optradio1 !!}</span></input><br>

        <button type="submit" class="btn btn-primary col-2 text-center ">Confirmar</button>
</center>
{!!Form:: close()!!}
<hr>
<br>
<br>


<div class="row mb-5">
        <div class="col-md">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Vegefoods</h2>
            <p>Saúde em primeiro lugar</p>
            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-5">
            <h2 class="ftco-heading-2">Menu</h2>
            <ul class="list-unstyled">
            <li><a href="/loja" class="py-2 d-block">Loja</a></li>
            <li><a href="/sobre" class="py-2 d-block">Sobre</a></li>
            <li><a href="/blog" class="py-2 d-block">Blog</a></li>
            <li><a href="/contato" class="py-2 d-block">Contato</a></li>
            </ul>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md-4">
            <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Ajuda</h2>
            <div class="d-flex">
                <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                <li><a href="#" class="py-2 d-block">Informação de envio</a></li>
                <li><a href="#" class="py-2 d-block">Retorno &amp; Troca</a></li>
                <li><a href="#" class="py-2 d-block">Termos &amp; Condições</a></li>
                <li><a href="#" class="py-2 d-block">Política de Privacidade</a></li>
                </ul>
                <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">FAQs</a></li>
                <li><a href="#" class="py-2 d-block">Contato</a></li>
                </ul>
            </div>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Faça uma pergunta?</h2>
            <div class="block-23 mb-3">
                <ul>
                <li><span class="icon icon-map-marker"></span><span class="text">Av.Pontes,37 Ipira BA </span></li>
                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(71)993775830</span></a></li>
                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">Vegefoods@email.com</span></a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    <!------------------------------------------------------------------------->
    <div class="row">
        <div class="col-md-12 text-center">
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados | Vegefoods desenvolvido por <a href="linkedin.com/in/sandoelio-silva" target="_blank">Sandoélio</a>
        </div>
    </div>
    </div>
</footer>
<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
<!-- Fim loader -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="js/google-map.js"></script>
<script src="js/main.js"></script>

</body>
</html>
