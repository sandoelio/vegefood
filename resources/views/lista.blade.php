@include('cabecalho')

<a href="index">
    <div class="hero-wrap hero-bread" style="background-image: url('images/bg_1.jpg');">
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-0 bread">Boas Compras</h1>
          </div>
        </div>
      </div>
    </div>
</a>
@include('menu')
    <!---------------Fim-Menu------------------------------->
<form action="/verificacao" method="post" >
    {!! csrf_field() !!}
<div class="container">
    <div class="row">
    <div class="col-md-12 ftco-animate">
        <div class="cart-list">
            <table class="table">
                <thead class="thead-primary">
                    <tr class="text-center">
                    <th>&nbsp;</th>
                    <th>Produto</th>
                    <th>&nbsp;</th>
                    <th>Preço</th>
                    <th>Quantidade</th>
                    <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox1" name="blankCheckbox1" value="Pimentão" aria-label="...">
                    </div>
                </td>
                <td class="image-prod"><div class="img" style="background-image:url(images/product-1.jpg);"></div></td>

                <td class="product-name">
                    <h3>Pimentão</h3>
                    <p>100 unidades por:</p>
                </td>
                <td class="price">
                    R$<var id="valorUm">80,00</var>
                </td>
                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoUm" id="campoUm" class="quantity form-control">
                </td>
                    <td class="total">R$<span id="totalCampoUm" name="totalCampoUm"></span>
                    <input type="hidden" id="totalImput1" name="totalImput1"></input>
                </td>
                </tr>
                <!---------------------- END TR---------------------------->

                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox2" name="blankCheckbox2" value="Morango" aria-label="...">
                    </div>
                </td>

                <td class="image-prod"><div class="img" style="background-image:url(images/product-2.jpg);"></div></td>

                <td class="product-name">
                    <h3>Morango</h3>
                    <p>100 unidades por:</p>
                </td>

                <td class="price">
                    R$<var id="valorDois">120,00</var>
                </td>

                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoDois" id="campoDois" class="quantity form-control">
                </div>
                </td>

                <td class="total">R$<span id="totalCampoDois"></span>
                    <input type="hidden" id="totalImput2" name="totalImput2"></input>
                </td>
                </tr>
                <!---------------------- END TR---------------------------->
                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox3" name="blankCheckbox3" value="Feijão Verde" aria-label="...">
                    </div>
                </td>

                <td class="image-prod"><div class="img" style="background-image:url(images/product-3.jpg);"></div></td>

                <td class="product-name">
                    <h3>Feijão Verde</h3>
                    <p>100 unidades por:</p>
                </td>

                <td class="price">
                    R$<var id="valorTres">120,00</var>
                </td>

                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoTres" id="campoTres" class="quantity form-control">
                </div>
                </td>

                <td class="total">R$<span id="totalCampoTres"></span>
                    <input type="hidden" id="totalImput3" name="totalImput3"></input>
                </td>
                </tr>
                <!---------------------- END TR---------------------------->
                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox4" name="blankCheckbox4" value="Repolho Roxo" aria-label="...">
                    </div>
                </td>

                <td class="image-prod"><div class="img" style="background-image:url(images/product-4.jpg);"></div></td>

                <td class="product-name">
                    <h3>Repolho Roxo</h3>
                    <p>100 unidades por:</p>
                </td>

                <td class="price">
                    R$<var id="valorQuatro">120,00</var>
                </td>

                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoQuatro" id="campoQuatro" class="quantity form-control">
                </div>
                </td>

                <td class="total">R$<span id="totalCampoQuatro"></span>
                    <input type="hidden" id="totalImput4" name="totalImput4"></input>
                </td>
                </tr>
                <!---------------------- END TR---------------------------->
                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox5" name="blankCheckbox5" value="Tomate" aria-label="...">
                    </div>
                </td>

                <td class="image-prod"><div class="img" style="background-image:url(images/product-5.jpg);"></div></td>

                <td class="product-name">
                    <h3>Tomate</h3>
                    <p>100 unidades por:</p>
                </td>

                <td class="price">
                    R$<var id="valorCinco">120,00</var>
                </td>

                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoCinco" id="campoCinco" class="quantity form-control">
                </div>
                </td>

                <td class="total">R$<span id="totalCampoCinco"></span>
                    <input type="hidden" id="totalImput5" name="totalImput5"></input>
                </td>
                </tr>
                <!---------------------- END TR---------------------------->
                <tr class="text-center">
                <td>
                    <div class="form-check">
                        <input class="form-check-input position-static" type="checkbox" id="blankCheckbox6" name="blankCheckbox6" value="Couve Flor" aria-label="...">
                    </div>
                </td>

                <td class="image-prod"><div class="img" style="background-image:url(images/product-6.jpg);"></div></td>

                <td class="product-name">
                    <h3>Couve Flor</h3>
                    <p>100 unidades por:</p>
                </td>

                <td class="price">
                    R$<var id="valorSeis">120,00</var>
                </td>

                <td class="quantity">
                    <div class="input-group mb-3">
                    <input onchange="somaTudo()" type="number" min="0"name="campoSeis" id="campoSeis" class="quantity form-control">
                </div>
                </td>
                <td class="total">R$<span id="totalCampoSeis"></span>
                    <input type="hidden" id="totalImput6" name="totalImput6"></input>
                </td>

            </tr>
            <!---------------------- END TR---------------------------->
        </tbody>
    </table>
    <!---------------------- SUBTOTAL span---------------------------->
    <td>
        <div class="coluna">
            Subtotal:  R$  <span id="subtotalSpan" name="subtotalSpan"></span>
        </div>
    </td>
    <br>
        <!------------imput para captar subtotal---------------------->
        <td>
            <input type="hidden" id="subtotalImput" name="subtotalImput"></input>
        </td>

    </div>
</div>
</div>
    <br>
    <!--<div class="col text-center">
            <div class="block-27">
            <ul>
                <li><a href="#">&lt;</a></li>
                <li class="active"><span>1</span></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#">&gt;</a></li>
            </ul>
            </div>
        </div>-->
            <center>
                <button type="submit" class="btn btn-primary col-5 text-center">Enviar seu Pedido</button>
            </center>
</form>
</section>

@include('rodape')

<script>
    function somaTudo(){
    var valorUm     = parseInt(document.getElementById("campoUm").value)
    var valorDois   = parseInt(document.getElementById("campoDois").value)
    var valorTres   = parseInt(document.getElementById("campoTres").value)
    var valorQuatro = parseInt(document.getElementById("campoQuatro").value)
    var valorCinco  = parseInt(document.getElementById("campoCinco").value)
    var valorSeis   = parseInt(document.getElementById("campoSeis").value)
    //-------------------------------------------------------
    if(isNaN(valorUm))
    valorUm = 0

    if(isNaN(valorDois))
    valorDois = 0

    if(isNaN(valorTres))
    valorTres = 0

    if(isNaN(valorQuatro))
    valorQuatro = 0

    if(isNaN(valorCinco))
    valorCinco = 0

    if(isNaN(valorSeis))
    valorSeis = 0
    //-------------------------------------------------------
    var totalUm     = (valorUm * 80.00 + ",00");
    var totalDois   = (valorDois * 120.00 + ",00");
    var totalTres   = (valorTres * 120.00 + ",00");
    var totalQuatro = (valorQuatro * 120.00 + ",00");
    var totalCinco  = (valorCinco * 120.00 + ",00");
    var totalSeis   = (valorSeis * 120.00 + ",00");
    var total = parseInt(totalUm) + parseInt(totalDois) +
                parseInt(totalTres) + parseInt(totalQuatro) +
                parseInt(totalCinco) + parseInt(totalSeis);
    //--------------------total dos span------------------------
    document.getElementById("totalCampoUm").innerHTML= totalUm;
    document.getElementById("totalCampoDois").innerHTML = totalDois;
    document.getElementById("totalCampoTres").innerHTML = totalTres;
    document.getElementById("totalCampoQuatro").innerHTML = totalQuatro;
    document.getElementById("totalCampoCinco").innerHTML = totalCinco;
    document.getElementById("totalCampoSeis").innerHTML = totalSeis;
    //------------------------subtotal--------------------------
    document.getElementById("subtotalSpan").innerHTML = total + ",00";
    document.getElementById("subtotalImput").value = total + ",00";
    //-------------total do inputs hidden-----------------------
    document.getElementById("totalImput6").value = totalSeis;
    document.getElementById("totalImput5").value = totalCinco;
    document.getElementById("totalImput4").value = totalQuatro;
    document.getElementById("totalImput3").value = totalTres;
    document.getElementById("totalImput2").value = totalDois;
    document.getElementById("totalImput1").value = totalUm;
    }
</script>

</body>
</html>
