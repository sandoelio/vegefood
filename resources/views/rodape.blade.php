<!-- Botão topo-->
<footer class="ftco-footer ftco-section">
    <div class="container">
    <div class="row">
        <div class="mouse">
            <a href="#" class="mouse-icon">
                <div class="mouse-wheel"><span class="ion-ios-arrow-up"></span></div>
            </a>
        </div>
    </div>
<!-- Fim Botão topo-->
    <div class="row mb-5">
        <div class="col-md">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Vegefoods</h2>
            <p>Saúde em primeiro lugar</p>
            <ul class="ftco-footer-social list-unstyled float-md-left float-lft mt-5">
            <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
            <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
            </ul>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md">
        <div class="ftco-footer-widget mb-4 ml-md-5">
            <h2 class="ftco-heading-2">Menu</h2>
            <ul class="list-unstyled">
            <li><a href="/loja" class="py-2 d-block">Loja</a></li>
            <li><a href="/sobre" class="py-2 d-block">Sobre</a></li>
            <li><a href="/blog" class="py-2 d-block">Blog</a></li>
            <li><a href="/contato" class="py-2 d-block">Contato</a></li>
            </ul>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md-4">
            <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Ajuda</h2>
            <div class="d-flex">
                <ul class="list-unstyled mr-l-5 pr-l-3 mr-4">
                <li><a href="#" class="py-2 d-block">Informação de envio</a></li>
                <li><a href="#" class="py-2 d-block">Retorno &amp; Troca</a></li>
                <li><a href="#" class="py-2 d-block">Termos &amp; Condições</a></li>
                <li><a href="#" class="py-2 d-block">Política de Privacidade</a></li>
                </ul>
                <ul class="list-unstyled">
                <li><a href="#" class="py-2 d-block">FAQs</a></li>
                <li><a href="#" class="py-2 d-block">Contato</a></li>
                </ul>
            </div>
        </div>
        </div>
    <!------------------------------------------------------------------------->
        <div class="col-md">
        <div class="ftco-footer-widget mb-4">
            <h2 class="ftco-heading-2">Faça uma pergunta?</h2>
            <div class="block-23 mb-3">
                <ul>
                <li><span class="icon icon-map-marker"></span><span class="text">Rua oito de Novembro,27 </span></li>
                <li><a href="#"><span class="icon icon-phone"></span><span class="text">(71)988235830</span></a></li>
                <li><a href="#"><span class="icon icon-envelope"></span><span class="text">Vegefoods@gmail.com</span></a></li>
                </ul>
            </div>
        </div>
        </div>
    </div>
    <!------------------------------------------------------------------------->
    <div class="row">
        <div class="col-md-12 text-center">
            Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos os direitos reservados | Vegefoods desenvolvido por <a href="linkedin.com/in/sandoelio-silva" target="_blank">Sandoélio</a>
        </div>
    </div>
    </div>
</footer>
<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
<!-- Fim loader -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-3.0.1.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.stellar.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/aos.js"></script>
<script src="js/jquery.animateNumber.min.js"></script>
<script src="js/bootstrap-datepicker.js"></script>
<script src="js/scrollax.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
<script src="js/google-map.js"></script>
<script src="js/main.js"></script>

</body>
</html>
