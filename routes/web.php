<?php

//rota relacionada a pagina inicial
Route::get('/', function () {
    return view('index');
});

//rota relacionada a logomarca
Route::get('/index', function () {
    return view('index');
});

//rota relacionada ao dropdown
Route::get('/loja', function () {
    return view('loja');
});
Route::get('/lista', function () {
    return view('lista');
});
Route::get('/produto', function () {
    return view('produto');
});
Route::get('/carrinho', function () {
    return view('carrinho');
});
Route::get('/verificacao', function () {
    return view('verificacao');
});
//rota relacionada ao sobre
Route::get('/sobre', function () {
    return view('sobre');
});
//rota relacionada ao blog
Route::get('/blog', function () {
    return view('blog');
});
//rota relacionada ao contato
Route::get('/contato', function () {
    return view('contato');
});

Route::get('/blog-single', function () {
    return view('blog-single');
});

Route::get('/frutas', function () {
    return view('frutas');
});


Route::post('/verificacao','ProdutoController@store');
Route::post('/dados','ProdutoController@storeVerificacao');
Route::post('/envio','EmailController@store')->name('mail.store');
